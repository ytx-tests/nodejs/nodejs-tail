const fs = require('fs');
const path = require('path');

const chokidar = require('chokidar');

const Tail = require('tail').Tail;

/** @typedef { (filename: string) => ((data: any) => any) } LineHandler */
/** @typedef { (filename: string) => ((error: any) => any) } ErrorHandler */

/** A map that manages files to watch. */
class WatchingFilesMap {
    /** @type { Map<string, Tail> } */
    #watchers = new Map();

    /** @type { LineHandler } */
    #lineHandler;

    /** @type { ErrorHandler } */
    #errorHandler;

    /**
     * Construct a new watching files map, with custom line and error handlers.
     * @param { ?LineHandler } lineHandler
     * @param { ?ErrorHandler } errorHandler
     */
    constructor(lineHandler = null, errorHandler = null) {
        this.#lineHandler = (lineHandler || this.#defaultLineHandler).bind(this);
        this.#errorHandler = (errorHandler || this.#defaultErrorHandler).bind(this);
    }

    /** @param { string } filename */
    static #createFileIfNotExist(filename) {
        filename = path.resolve(filename);

        try {
            fs.statSync(filename);
        }
        catch {
            fs.mkdirSync(path.dirname(filename));

            const fd = fs.openSync(filename, 'w');
            fs.closeSync(fd);
        }
    }

    /** @param { string } filename */
    #createTailForFile(filename) {
        filename = path.resolve(filename);

        WatchingFilesMap.#createFileIfNotExist(filename);

        if (this.#watchers.has(filename)) {
            this.#watchers.get(filename).unwatch();
        }

        // Reference: https://github.com/lucagrulla/node-tail#use

        let tail = new Tail(filename);

        tail.on('line', this.#lineHandler(filename));

        tail.on('error', (error) => {
            // The affected file must be unwatched
            // regardless of the user's custom handler.
            this.rmFileFromWatch(filename);

            this.#errorHandler(filename)(error);
        });

        this.#watchers.set(filename, tail);

        return tail;
    }

    /** @type { LineHandler } */
    #defaultLineHandler(filename) {
        filename = path.resolve(filename);
        return (data) => {
            console.log(`[NEW LINE from \"${filename}\"] ${data}`);
        }
    }

    /** @type { ErrorHandler } */
    #defaultErrorHandler(filename) {
        filename = path.resolve(filename);
        return (error) => {
            console.error(`[ERROR from \"${filename}\"] ${error}`);
        }
    }

    /** @param { string } filename */
    addFileToWatch(filename) {
        filename = path.resolve(filename);
        this.#createTailForFile(filename);
    }

    /** @param { string } filename */
    rmFileFromWatch(filename) {
        filename = path.resolve(filename);

        if (!this.#watchers.has(filename)) {
            return;
        }

        this.#watchers.get(filename).unwatch();
        this.#watchers.delete(filename);
    }
}

/**
 * A watcher class that allows you to specify files to watch,
 * as well as custom line handling and error handling functions.
 */
class Watcher {
    /** @type { WatchingFilesMap } */
    #watchingFilesMap;

    /** @type { readonly chokidar.FSWatcher } */
    #watcher;

    /** @type { import('chokidar').WatchOptions } */
    static defaultOptions = {
        persistent: true
    };

    /**
     * Construct a custom watcher.
     *
     * **[NOTE] Currently `chokidar` has bugs when handling symbolic links:**
     * - For symlink directories, **ANY** files under that directory and its
     * subdirectories will be watched.
     * - For symlink files, if their directory prefixes match the glob, they will
     * be watched **regardless of** the globs.
     *
     * See: https://github.com/paulmillr/chokidar/issues/967
     *
     * @param { string | readonly string[] } globs a string or an array of strings specifying paths to watch.
     * @param { import('chokidar').WatchOptions } options options for watching files.
     * @param { ?LineHandler } lineHandler the handler to be fired when a
     * new line appears in any file being watched.
     * @param { ?ErrorHandler } errorHandler the handler to be fired when an
     * error occurs in any file being watched.
     */
    constructor(globs, options = Watcher.defaultOptions,
            lineHandler = null, errorHandler = null) {

        this.#watcher = chokidar.watch(globs, options);

        this.#watcher.on('add', this.#handleAdd.bind(this));
        this.#watcher.on('unlink', this.#handleUnlink.bind(this));

        this.#watchingFilesMap = new WatchingFilesMap(lineHandler, errorHandler);
    }

    /** @param { string } path */
    #handleAdd(path) {
        this.#watchingFilesMap.addFileToWatch(path);
        console.info(`[ADDED] path "${path}" for tail watching.`);
    }

    /** @param { string } path */
    #handleUnlink(path) {
        this.#watchingFilesMap.rmFileFromWatch(path);
        console.info(`[DELETED] "${path}" from tail watching.`);
    }
}

module.exports = Watcher;