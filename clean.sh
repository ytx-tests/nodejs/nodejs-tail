#!/usr/bin/bash

find . -path "./logs/*" -type d -prune -exec rm -rf {} ";"