# Node.js Tail (ping)

This repository is used for testing growing logs that result from the `ping` command. The following domain names are used for testing:

1. [example.com](http://example.com)
2. [baidu.com](https://baidu.com)
3. [qq.com](https://qq.com)
4. [toutiao.com](https://toutiao.com)
5. [taobao.com](https://taobao.com)


## Install dependencies
```bash
npm install
```

## How to run
1. In one terminal window, start watching file changes with Node.js:
    ```bash
    node index.js
    ```
2. In another terminal window, start the ping script:
    ```bash
    bash ping.sh
    ```
    * This script will write the output logs into the [`logs`](./logs/) directory. Each output file is named as `logs/<DOMAIN_NAME>/ping.log`. For example, the ping results for [`example.com`](http://example.com) is logged into the file`logs/example.com/ping.log`.
    * The terminal that holds the watcher (as started in step 1) will show the lines written into the log files in real time.
3. After finishing, go to the watcher's terminal window (as started in step 1), press `^C` to quit.
4. To clean up all generated log files, run:
    ```
    bash clean.sh
    ```

## Dependencies
* [lucagrulla/node-tail](https://github.com/lucagrulla/node-tail)
* [paulmillr/chokidar](https://github.com/paulmillr/chokidar)

## Notes

**What happens if you delete the log files while `ping.sh` is still running?**

In this situation, each process that invokes each `ping` command involved in `ping.sh` still retains a *file descriptor* to the file opened in memory. When you delete that log file in the directory, it is gone from the directory. However, since there are still *opened handles* (*file descriptors*) of this file in the memory, the `ping` process will still write output into that opened file descriptor. The remaining output is simply not visible to you because the file is no longer in the directory.

**What if you recreate the file under the same directory with the same filename?**

The remaining output will *not* get to the newly created file, because the file descriptor held by the existing `ping` process does *not* link to the new file under the same name.