const path = require('node:path');

const Watcher = require('./watcher');

const OUTPUT_DIR = path.join(__dirname, 'logs');
const OUTPUT_GLOB = `${OUTPUT_DIR}/**/*.log`;

console.info(`Watching directory: "${OUTPUT_DIR}"`);

new Watcher(OUTPUT_GLOB);