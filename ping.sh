#!/usr/bin/bash

TEST_DOMAINS=(example.com baidu.com qq.com toutiao.com taobao.com)
TEST_PACKETS=5

CURDIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
LOGDIR="$CURDIR/logs"

function ping_all {
    for TEST_DOMAIN in ${TEST_DOMAINS[@]}
    do
        local OUTPUT_FILE="$LOGDIR/$TEST_DOMAIN/ping.log"
        mkdir -p "$(dirname "$OUTPUT_FILE")"

        ping -c $TEST_PACKETS $TEST_DOMAIN | tee "$OUTPUT_FILE" &
    done

    wait
}

ping_all